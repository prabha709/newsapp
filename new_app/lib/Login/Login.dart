import 'package:new_app/App_Colors/App_Colors.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:email_validator/email_validator.dart';
import 'package:new_app/slidebar/slidebar_layout.dart';
import 'package:new_app/utils/methods.dart';
import 'package:new_app/utils/raised_button.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  bool loading = false;
  bool passWordVisible = true;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }

  String _email;
  String _password;

  Future<void> _navigatetoForgetpassword() async{
     await FirebaseAuth.instance.sendPasswordResetEmail(email: _email);
  }

  Future<void> _logIn(context) async {
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();

      try {
        final AuthResult user =
            await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _email,
          password: _password,
        );
        //  Methods.showSnackBar("Logedin Sucessfully", context);
        Navigator.pop(context);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SlibeBarLayout()));
      } catch (error) {
        setState(() {
          loading = false;
        });
        Methods.showSnackBar(error.message, context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
     



    final email = Transform.translate(
        offset: Offset(0.0, 70.0),
        child: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.person),
                hintText: 'Enter Your Email',
                labelText: "Email",
              ),
              validator: (val) => !EmailValidator.validate(val, true)
                  ? 'Not a Valid Email.'
                  : null,
              onSaved: (val) => _email = val,
            )
            )
            );

    final password = Transform.translate(
        offset: Offset(0.0, 140.0),
        child: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: TextFormField(
              obscureText: true,
              autofocus: false,
              decoration: InputDecoration(
                hintText: 'Enter Your Password',
                labelText: "Password",
              ),
              validator: (val) =>
                  val.length < 4 ? 'Password too Short..' : null,
              onSaved: (val) => _password = val,
            )));

    final loginButton = Transform.translate(
        offset: Offset(0.0, 220.0),
        child: Padding(
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          child: MyRaisedButton(
            loading: loading,
            onPressed: _logIn,
            title: "Login",
          ),
        )
        );

        final forgotlabel =Transform.translate(
        offset: Offset(0.0, 300.0),
        child: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: Center(
                child: InkWell(
              onTap: () {
            //     AlertDialog(
            //       title: Text("Reset Password"),
            //       content: Column(
            //         children: <Widget>[
            //           Text("Please enter your mail id"),
            //           TextFormField(
            //   keyboardType: TextInputType.emailAddress,
            //   autofocus: false,
            //   decoration: InputDecoration(
            //     hintText: 'Enter Your Email',
            //     labelText: "Email",
            //   ),
            //   validator: (val) => !EmailValidator.validate(val, true)
            //       ? 'Not a Valid Email.'
            //       : null,
            //   onSaved: (val) => _email = val,
            // )
            //         ],
            //       ),


            //     );
                 _navigatetoForgetpassword();
              },
              child: Text(
                "Forget Your Password?",
                style: TextStyle(
                    fontSize: 15, color: Theme.of(context).buttonColor),
              ),
            ))));

    return Scaffold(
        appBar: AppBar(
         iconTheme: IconThemeData(color: colorBrown),
          centerTitle: true,
          title: Text(
            "Login",
            style: TextStyle(color: colorBrown),
          ),
          backgroundColor: colorWhite,
        ),
        body: new Form(
          key: _formkey,
          child: new ListView(
            shrinkWrap: true, 
            children: <Widget>[
            SizedBox(
              height: 10.0,
            ),
            Center(
              child: Image.asset('assets/logo.png',
                  width: MediaQuery.of(context).size.width / 3.2),
            ),
            Stack(children: <Widget>[
              LoginBlueRectangle(),
              email,
              password,
              loginButton,
              forgotlabel,
            ]),
            
          ]),
        ));
  }
}

class LoginBlueRectangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform(
        transform: Matrix4.skewY(0.1)..rotateX(3.14 / 20.0),
        child: Transform.translate(
          offset: Offset(0.0, 0.0),
          child: Container(
            height: MediaQuery.of(context).size.height / 2.2,

            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(0.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: pinkGradients,
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            // color: Theme.of(context).secondaryHeaderColor
          ),
        ));
  }
}




