import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:new_app/App_Colors/App_Colors.dart';
import 'package:new_app/Login/Login.dart';
import 'package:new_app/UserManagement/UserManagement.dart';
import 'package:new_app/utils/methods.dart';
import 'package:new_app/utils/raised_button.dart';

class NewRegister extends StatefulWidget {
  @override
  _NewRegisterState createState() => _NewRegisterState();
}

class _NewRegisterState extends State<NewRegister> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool loading = false;
  bool passWordVisible = true;
  void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }

  String _email;
  String _password;
  String _firstName;
  String _lastName;
  String _phoneNumber;

_navigateToLogin(){
  Methods.showSnackBar("Signed in Sucessfully", context);
 Navigator.pop(context);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Login()));
     
}

  Future<void> _signUp(context) async {
if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();

try {
        final AuthResult user = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
          email: _email,
          password: _password,
        )
            .then((signedInUser) {
          return UserManagement()
              .storeNewUser(signedInUser, _firstName, _lastName, _phoneNumber);
        })
        .whenComplete(_navigateToLogin);
        
        
        // Methods.showSnackBar("Signed in Sucessfully", context);
        // Navigator.pop(context);

        // Navigator.push(
            // context, MaterialPageRoute(builder: (context) => Login()));
      } catch (error) {
        setState(() {
          loading = false;
        });
        Methods.showSnackBar(error.message, context);
      }
  }
  }
  @override
  Widget build(BuildContext context) {



    final firstName = 
    Transform.translate(
      offset: Offset(0, 50),
    child: Padding(
      padding: const EdgeInsets.only(left:24.0,right: 24.0),
      child: TextFormField(
        
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          
          
          hintText: 'FirstName',
        ),
        validator: (String value){
          if(value.isEmpty){
            return "Please Enter the first name";
          }
          return null;
        },
        
        // (val) => val = null ? "please enter the First name" : null,
        onSaved: (String value) => _firstName = value,
      ),
    ),
    );
    
    
    final lastName =
    Transform.translate(
      offset: Offset(0, 110),
      child:Padding(
        padding: const EdgeInsets.only(left:24.0,right: 24.0),
        child: TextFormField(
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
          hintText: 'LastName',
          // border: OutlineInputBorder(
          //   borderSide: new BorderSide(color: Colors.teal),
          //   borderRadius: BorderRadius.circular(32.0),
          // ),
        ),
        validator:(String value){
          if(value.isEmpty){
            return "Please Enter the Last Name";

          }
          return null;

        } ,
        
        // (val) => val = null ? "please enter the last name" : null,
        onSaved: (value) => _lastName = value,
    ),
      )
      );
    
    
    final phoneNumber = Transform.translate(
        offset: Offset(0.0, 160.0),
        child: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: TextFormField(
              keyboardType: TextInputType.phone,
              autofocus: false,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.phone_android),
                  hintText: 'Enter Phone Number', labelText: "Phone Number"),
              validator: (val) => val.length < 10
                  ? "Please enter the valid mobile number"
                  : null,
              onSaved: (val) => _phoneNumber = val,
            )));
    final email = Transform.translate(
        offset: Offset(0.0,220.0),
        child: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: TextFormField(

              keyboardType: TextInputType.emailAddress,
              autofocus: false,
              decoration: InputDecoration(
suffixIcon: Icon(Icons.email),
                hintText: 'Enter Your Email',
                labelText: "Email",
              ),
              validator: (val) => !EmailValidator.validate(val, true)
                  ? 'Not a Valid Email.'
                  : null,
              onSaved: (val) => _email = val,
            )));

    final password = Transform.translate(
        offset: Offset(0.0, 280.0),
        child: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: TextFormField(
              obscureText: passWordVisible,
              autofocus: false,
              decoration: InputDecoration(
                 suffixIcon: IconButton(icon: Icon(
          passWordVisible? Icons.visibility
          : Icons.visibility_off,
          // color: Theme.of(context).primaryColor,
        ),
        onPressed:togglePasswordVisiblity,
        iconSize:25.0,),
                hintText: 'Enter Your Password',
                labelText: "Password",
              ),
              validator: (val) =>
                  val.length < 4 ? 'Password too Short..' : null,
              onSaved: (val) => _password = val,
            )));

    final loginButton = Transform.translate(
        offset: Offset(50.0, 390.0),
        child: Padding(
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          // child: RaisedButton(
            // child: Text("SignUp"),

            // onPressed: _login),
          child: MyRaisedButton(
            loading: loading,
            onPressed: _signUp,
            title: "SignUp",
          ),
        ));
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: colorBrown),
        centerTitle: true,
        title: Text(
          "Sign Up",
          style: TextStyle(color: colorBrown),
        ),
      ),
      body: new Form(
        key: _formkey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Center(
              child: Image.asset('assets/logo.png',
                  width: MediaQuery.of(context).size.width / 3.2),
            ),
            Stack(
              children: <Widget>[
                Transform(
        transform: Matrix4.skewY(0.1)..rotateX(3.14 / 20.0),
        child: Transform.translate(
          offset: Offset(0.0, 0.0),
          child: Container(
            height: MediaQuery.of(context).size.height / 1.8,

            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.all(0.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: pinkGradients,
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            // color: Theme.of(context).secondaryHeaderColor
          ),
        )),
                firstName,
               lastName,
               phoneNumber,
                 email,
                 password,
                 loginButton,
                 
              ],
            ),
            Transform.translate(
      offset: Offset(0.0, 30.0),
      child: Padding(
        padding: EdgeInsets.only(left: 24.0, right: 24.0),
        child: Center(
          child: FlatButton(
            child: Text(
              "Already have an account Login",
              style: TextStyle(color: colorBrown),
            ),
            onPressed: () {},
          ),
        ),
      ),
    )

          ],
        ),
      ),
      
    );
  }
}