import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:new_app/screens/Welcome.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/material.dart';
import '../screens/AddNews.dart';
import '../screens/profile_page.dart';

class AuthenicationService{
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<bool> isUserLoggedIn() async{
    var user = await _firebaseAuth.currentUser();
    return user != null;
  }
}

class SideBar extends StatefulWidget {
SideBar({
  this.user
});
final FirebaseUser user;

  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar>
    with SingleTickerProviderStateMixin<SideBar> {
  AnimationController _animationController;
  StreamController<bool> isSideBarOpenedStreamController;
  Stream<bool> isSideBarOpenedStream;
  StreamSink<bool> isSideBarOpenedSink;

  final _animationDuration = Duration(milliseconds: 500);

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: _animationDuration);
    isSideBarOpenedStreamController = PublishSubject<bool>();
    isSideBarOpenedStream = isSideBarOpenedStreamController.stream;
    isSideBarOpenedSink = isSideBarOpenedStreamController.sink;
  }

  @override
  void dispose() {
    _animationController.dispose();
    isSideBarOpenedStreamController.close();
    isSideBarOpenedSink.close();
    super.dispose();
  }

  void onIconPressed() {
    final animationStatus = _animationController.status;
    final isAnimationCompleted = animationStatus == AnimationStatus.completed;

    if (isAnimationCompleted) {
      isSideBarOpenedSink.add(false);
      _animationController.reverse();
    } else {
      isSideBarOpenedSink.add(true);
      _animationController.forward();
    }
  }
void _login(){
  isSideBarOpenedSink.add(false);
      _animationController.reverse();
   Navigator.push(context, MaterialPageRoute(builder: (context)=>Welcome() ));
}

void _navigatetoAddnews(){
  isSideBarOpenedSink.add(false);
      _animationController.reverse();
  Navigator.push(context, MaterialPageRoute(builder: (context)=>AddNews() ));

}
void navigatetoSetting(){
  isSideBarOpenedSink.add(false);
      _animationController.reverse();
  Navigator.push(context, MaterialPageRoute(builder: (context)=>ProfilePage() ));

}
void navigatetoHome(){
  isSideBarOpenedSink.add(false);
      _animationController.reverse();

}

  @override
  Widget build(BuildContext context) {
    
    final AuthenicationService _authenticationService = AuthenicationService();
     
     var hasLoggedUser = _authenticationService.isUserLoggedIn();

final _loginUser = Text("Data",style: TextStyle(color: Colors.orange),);

    final screenWidth = MediaQuery.of(context).size.width;
    return StreamBuilder<bool>(
      initialData: false,
      stream: isSideBarOpenedStream,
      builder: (context, isSideBarOpenedAsync) {
        return AnimatedPositioned(
          duration: _animationDuration,
          bottom: 0,
          top: 0,
          left: isSideBarOpenedAsync.data ? 0 : -screenWidth,
          right: isSideBarOpenedAsync.data ? 0 : screenWidth - 45,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Colors.purple,
                  child: Column(
                    children: <Widget>[
                      Container(
                        // color: Colors.amber,
                        height: MediaQuery.of(context).size.height/2.5,
                        width: MediaQuery.of(context).size.width,
                        child: Center(
                          child: Image.asset("assets/Logo.png"),
                        ),
                      ),
                      Divider(thickness: 1.2,),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: _loginUser
                        // Text("Data",style: TextStyle(color: Colors.white),)
                        ),
                      
                      InkWell(
                        onTap: (){
                          navigatetoHome();
                        },
                                              child: Padding(
                          padding: const EdgeInsets.only(left:20.0),
                          child: ListTile(
                            leading: Icon(Icons.home,size: 35,),
                            title: Text("Home",style: TextStyle(fontSize: 20, color: Color(0xffb4f554),fontWeight: FontWeight.bold),),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          _login();
                          },
                                              child: Padding(
                          padding: const EdgeInsets.only(left:20.0),
                          child: ListTile(
                            
                            leading: Icon(Icons.vpn_key,size: 35,),
                            title: Text("Login/Signup",style: TextStyle(fontSize: 20, color: Color(0xffb4f554),fontWeight: FontWeight.bold),),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          _navigatetoAddnews();
                        },
                                              child: Padding(
                          padding: const EdgeInsets.only(left:20.0),
                          child: ListTile(
                            leading: Icon(Icons.add_circle,size: 35,),
                            title: Text("Add News",style: TextStyle(fontSize: 20, color: Color(0xffb4f554),fontWeight: FontWeight.bold),),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:20.0),
                        child: ListTile(
                          leading: Icon(Icons.accessibility,size: 35,),
                          title: Text("About Us",style: TextStyle(fontSize: 20, color: Color(0xffb4f554),fontWeight: FontWeight.bold),),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          navigatetoSetting();
                        },
                                              child: Padding(
                          padding: const EdgeInsets.only(left:20.0),
                          child: ListTile(
                            leading: Icon(Icons.settings,size: 35,),
                            title: Text("Settings",style: TextStyle(fontSize: 20, color: Color(0xffb4f554),fontWeight: FontWeight.bold),),
                          ),
                        ),
                      ),


                      Padding(
                        padding: const EdgeInsets.only(left:20.0),
                        child: ListTile(
                          leading: Icon(Icons.note,size: 35,),
                          title: Text("FeedBack",style: TextStyle(fontSize: 20, color: Color(0xffb4f554),fontWeight: FontWeight.bold),),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, -0.9),
                child: GestureDetector(
                  onTap: () {
                    onIconPressed();
                  },
                  child: ClipPath(
                    clipper: CustomMenuClipper(),
                                      child: Container(
                      height: 110,
                      width: 35,
                      color: Colors.purple,
                      child: Align(
                        alignment: Alignment.center,
                        child: AnimatedIcon(
                          progress: _animationController.view,
                          icon: AnimatedIcons.menu_close,
                          color: Color(0xffb4f554),
                          size: 25,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class CustomMenuClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size){
  Paint paint = Paint();
  paint.color = Colors.purple;
  final width = size.width;
  final height = size.height;

  

Path path = Path();
path.moveTo(0 ,0);
path.quadraticBezierTo(0, 8, 10, 16);
path.quadraticBezierTo(width-1, height /2-20, width, height/2);
path.quadraticBezierTo(width+1, height/2+20, 10, height-16);
path.quadraticBezierTo(0, height-8, 0, height);
path.close();

    return path;
  }
   @override
   bool shouldReclip(CustomClipper<Path>oldClipper){
     return true;
   }

}