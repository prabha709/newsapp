import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:new_app/App_Colors/App_Colors.dart';
import 'package:new_app/screens/AddNews.dart';
import 'package:new_app/utils/methods.dart';
import '../App_Colors/App_Colors.dart';
import 'package:cached_network_image/cached_network_image.dart';


class Cards extends StatelessWidget {
  final DocumentSnapshot viewModel;
  final double parellaxPercent;

  const Cards({Key key, this.viewModel, this.parellaxPercent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void snackbarCheck(){
      Methods.showSnackBar("Data Submitted Sucessfully", context);

    }
    
    final translation = FractionalTranslation(
      translation: Offset(parellaxPercent * 2.0, 0.0),
    );
    final whiteContainer =   Padding(
        padding: EdgeInsets.only(top: 30.0, left: 10.0),
        child: new Container(
          decoration: BoxDecoration(
            color:colorWhite,
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black,
                offset: Offset(1.0, 5.0),
                blurRadius: 25.0
              )
            ]
            

          ),
          
          height: MediaQuery.of(context).size.height,
        )

        );

    return Stack(
      children: <Widget>[
        
        translation,
        whiteContainer,

        new Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left:10.0, top: 20.0),
              child: Container(
                
                height: MediaQuery.of(context).size.height/2.2,
                decoration: BoxDecoration(
                  // color: Colors.purple,
                  borderRadius: BorderRadius.circular(20.0)
                  

                ),
                child: Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: ClipPath(
                    clipper: MyClipper(),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),

                                        child: CachedNetworkImage(
                        imageUrl:viewModel["image"],
                        errorWidget: (context,url,error){
                          return Icon(Icons.error);
                        },
                        placeholder: (context, url){
                          return SpinKitPumpingHeart(color: Colors.purple,);

                        },
                         ),
                    ),
                    ),
                ),
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(top: 20.0,left:10.0),
            //   child: ClipPath(
            //     clipper: MyClipper(),
            //     child: ClipRRect(
            //      borderRadius: BorderRadius.circular(20.0),
            //       child:CachedNetworkImage(
            //         imageUrl: viewModel["image"],
            //         errorWidget: (context,url,error){
            //         return Icon(Icons.error);
            //         },
            //         placeholder: (context,url){
            //           return SpinKitPumpingHeart(color: Colors.purple,);
                      
            //         },
                    
            //       ) 
                  
            //       // Image.network(viewModel["image"], )
            //       ),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.only(left:10.0),
              child: Container(
                // color: Colors.purple,
          height: MediaQuery.of(context).size.height/2.2,
          width: MediaQuery.of(context).size.width/1.1,
          child: new Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left:20.0,top: 10.0),
              child: new Text(
                viewModel['heading'],
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 26.0,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(left: 30.0, top: 20.0,right: 15.0),
                child: new Text(
                  viewModel['content'],
                  style: TextStyle(color: Colors.grey),
                )
                ),
                Spacer(),
                 Container(
                  //  color: Colors.purple,
                   child: Row(
                     children: <Widget>[
                       new Align(
          alignment: Alignment.bottomLeft,
          child: IconButton(
            icon: Icon(Icons.timelapse),
            onPressed: (){
              snackbarCheck();
            },
          )
        ),
        Column(
          children: <Widget>[
            new Text(
                      viewModel['newsdate'].toString(),
                      ),
                      //  new Text(
                      // viewModel['time'].toString(),
                      // ),
          ],
        ),
        Spacer(),
        Icon(Icons.comment),
        SizedBox(width: 10.0,),
        Icon(Icons.share),
        SizedBox(width: 10.0,),
        Padding(
          padding: const EdgeInsets.only(right:18.0),
          child: Icon(Icons.report),
        )

                     ],
                   ),
                 )
           
          ],
        ) ,
          // color: Colors.purple,
          
        ),
            )

          ],
        ),

        // content
        


     
                
           
        
       
       
      ],
    
    
    );

  }
}
