import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:ui' as prefix0;
import 'package:new_app/utils/methods.dart';
import 'package:new_app/news_Layout/Card.dart';

const String testDevice = 'Mobile_id';

class CardFlipper extends StatefulWidget {
  


 
  @override
  _CardFlipperState createState() => _CardFlipperState();
}

class _CardFlipperState extends State<CardFlipper>with TickerProviderStateMixin {
final FirebaseMessaging _fcm = FirebaseMessaging();
static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
      testDevices: testDevice != null ? <String>[testDevice] : null,
      nonPersonalizedAds: true,
      keywords: <String>['game', 'mario']
      );
      
  
 BannerAd _bannerAd;
  InterstitialAd _interstitialAd;

   BannerAd createBannerAd() {
    return BannerAd(
        adUnitId: BannerAd.testAdUnitId,
        size: AdSize.banner,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print('BannerAd $event');
        });
  }

  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
        adUnitId: InterstitialAd.testAdUnitId,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print('InterstitialAd $event');
        });
  }

Future getPosts() async {
    try {
      
      var firestore = Firestore.instance;
      QuerySnapshot qn = await firestore
      .collection('news')
      .orderBy("time", descending: true)
      .getDocuments();
      
      return qn.documents;
    } catch (e) {
      Methods.showSnackBar(e.message, context);
    }
  }
  List<DocumentSnapshot> cards =  new List();
 
double scrollPercent =0.0;
Offset startDrag;
double startDragPercentScroll;
double finishScrollStart;
double finishScrollEnd;
AnimationController finishScrollController;
@override
void initState(){
   FirebaseAdMob.instance.initialize(appId: BannerAd.testAdUnitId);
      // _interstitialAd = createInterstitialAd()..load()..show();
  super.initState();
  //  _fcm.configure(onMessage: (Map<String, dynamic> message) async {
  //     print('onmessage:$message');
  //   }, onResume: (Map<String, dynamic> message) async {
  //     print('onResume:$message');
  //   }, onLaunch: (Map<String, dynamic> message) async {
  //     print('onLaunch:$message');
  //   });
   getPosts().then((snap){
     setState(() {
       cards = snap;
     });
      
       finishScrollController = new AnimationController(
    duration: const Duration(milliseconds: 150),
    vsync: this,
  )
  ..addListener((){
setState(() {
  scrollPercent = prefix0.lerpDouble(finishScrollStart, finishScrollEnd, finishScrollController.value);

});
  });

    });
 
}

@override
void dispose(){
  finishScrollController.dispose();
  _bannerAd.dispose();
    _interstitialAd.dispose();
  super.dispose();
}




void _onHorizontalDragStart(DragStartDetails details){
  startDrag = details.globalPosition;
  startDragPercentScroll =scrollPercent;


}
void _onHorizontalDragUpdate(DragUpdateDetails details){
  final currDrag = details.globalPosition;
  final dragDistance= currDrag.dx- startDrag.dx;
  final singleCardDragPercent= dragDistance / context.size.width;
 
  setState(() {
    scrollPercent= (startDragPercentScroll+(-singleCardDragPercent/ cards.length)
    ).clamp(0.0, 1.0- (1/cards.length)); 
    
  });
  
}
void _onHorizontalDragEnd(DragEndDetails details){
 

  finishScrollStart= scrollPercent;
  finishScrollEnd =(scrollPercent * cards.length).round()/ cards.length;
  finishScrollController.forward(from: 0.0);
  setState(() {
    startDrag = null;
    startDragPercentScroll= null;
  });
  
}
  List<Widget>_buildCards(){

    final cardCount = cards.length;
int index =-1;
if(index % 5 == 0 ){
   _interstitialAd = createInterstitialAd()..load()..show();

} else{
   
     
     cards.map(( viewModel){
       ++index;
return _buildCard(viewModel, index, cardCount, scrollPercent);
     }).toList();

}
     
   
  }
  Matrix4 _buildCardProjection(double scrollPercent){
   // Pre-multiplied matrix of a projection matrix and a view matrix.
    //
    // Projection matrix is a simplified perspective matrix
    // http://web.iitd.ac.in/~hegde/cad/lecture/L9_persproj.pdf
    // in the form of
    // [[1.0, 0.0, 0.0, 0.0],
    //  [0.0, 1.0, 0.0, 0.0],
    //  [0.0, 0.0, 1.0, 0.0],
    //  [0.0, 0.0, -perspective, 1.0]]
    //
    // View matrix is a simplified camera view matrix.
    // Basically re-scales to keep object at original size at angle = 0 at
    // any radius in the form of
    // [[1.0, 0.0, 0.0, 0.0],
    //  [0.0, 1.0, 0.0, 0.0],
    //  [0.0, 0.0, 1.0, -radius],
    //  [0.0, 0.0, 0.0, 1.0]]
    final perspective = 0.002;
    final radius = 1.0;
    final angle = scrollPercent * 3.14 / 8;
    final horizontalTranslation = 0.0;
    Matrix4 projection = new Matrix4.identity()
      ..setEntry(0, 0, 1 / radius)
      ..setEntry(1, 1, 1 / radius)
      ..setEntry(3, 2, -perspective)
      ..setEntry(2, 3, -radius)
      ..setEntry(3, 3, perspective * radius + 1.0);

    // Model matrix by first translating the object from the origin of the world
    // by radius in the z axis and then rotating against the world.
    final rotationPointMultiplier = angle > 0.0 ? angle / angle.abs() : 1.0;
    print('Angle: $angle');
    projection *= new Matrix4.translationValues(
            horizontalTranslation + (rotationPointMultiplier * 300.0), 0.0, 0.0) *
        new Matrix4.rotationY(angle) *
        new Matrix4.translationValues(0.0, 0.0, radius) *
        new Matrix4.translationValues(-rotationPointMultiplier * 300.0, 0.0, 0.0);

    return projection;


  }

  Widget _buildCard( viewModel, int cardIndex, int cardCount, double scrollPercent){

    final cardScrollPercent = scrollPercent/(1/ cardCount);

    final parellax = scrollPercent-(cardIndex/cardCount );
    return FractionalTranslation(
      translation: Offset(cardIndex-cardScrollPercent, 0.0),

      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Transform(
          transform: _buildCardProjection(cardScrollPercent - cardIndex),
                  child: new Cards(

            viewModel:viewModel,
            parellaxPercent:parellax,
          ),
        ))
      );

  }
  @override
  Widget build(BuildContext context) {
    
    if(cards.length == 0){
      return Center(
        child:SpinKitChasingDots(color:Colors.purple)
      );
    }
    return GestureDetector(
      onHorizontalDragStart: _onHorizontalDragStart,
      onHorizontalDragUpdate: _onHorizontalDragUpdate,
      onHorizontalDragEnd: _onHorizontalDragEnd,
      behavior: HitTestBehavior.translucent,
          child: Stack(
        children: _buildCards(),
        
      ),
    );
    
  }
}