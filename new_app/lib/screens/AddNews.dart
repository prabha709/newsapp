import 'dart:io';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker_modern/image_picker_modern.dart';
import 'package:new_app/App_Colors/App_Colors.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:new_app/utils/methods.dart';
import 'package:new_app/utils/raised_button.dart';


class AddNews extends StatefulWidget {
  @override
  _AddNewsState createState() => _AddNewsState();
}

class _AddNewsState extends State<AddNews> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  
  File _image;
  bool loading = false;
  String _title;
  String _content;
  String url;
  void uploadImage(context)async{
 if(_formkey.currentState.validate()){
    setState(() {
        loading = true;
        
      });
  
    _formkey.currentState.save();
   
    try{
   
  final StorageReference postImageRef = FirebaseStorage.instance.ref().child('post Images');
  var timekey = new DateTime.now();
  final StorageUploadTask uploadTask = postImageRef.child(timekey.toString()+'.jpg').putFile(_image);
var imageUrl = await(await uploadTask.onComplete).ref.getDownloadURL();
url = imageUrl.toString();
print("image url = " + url); 
saveToDatabase(url);
    }catch(error){
  setState((){
    loading = false;
  });
  Methods.showSnackBar(error.message, context);
    }
}
}
void saveToDatabase(url){
  var dbtimekey = new DateTime.now();
  // var formatDate = new DateFormat('MMM d, yyyy');
   var formatTime = new DateFormat.Hm();
  // String date = formatDate.format(dbtimekey);
  String time = formatTime.format(dbtimekey);
  Firestore.instance.collection("/news").document().setData({
    'image':url,
     'newsdate':dbtimekey.toString(),
    "time":time.toString(),
    
    'heading':_title,
    'content':_content

  }).then((onValue){
     setState((){
    loading = false;
  });
  
  _image = null;
  _formkey.currentState.reset();
  Methods.showSnackBar("Data Submitted Sucessfully", context);

// Navigator.pop(context);
//  Navigator.push(context, MaterialPageRoute(builder: (context)=>Demo() ));

  });
 
}
  Future getImageFromGallery() async {
   
    // for gallery
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 600,
      maxHeight: 500,
    );
    var result = await FlutterImageCompress.compressAndGetFile(
      croppedFile.path, croppedFile.path,
      quality: 70,
      // rotate: 180,
    );
    setState(() {
      _image = result;
     
    });
  }

  @override
  Widget build(BuildContext context) {

    final headingField = Padding(
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: 'Enter the Title',
                    labelText: "Title",
                  ),
                  validator: (val)=>val.isEmpty?'Title is Required'
                  : null,
                  onSaved: (val) => _title= val,
                )
                );

    final contentField=Padding(
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                child: TextFormField(
                  // maxLines: 10,
                  autofocus: false,
                  decoration: InputDecoration(
                    hintText: 'Enter the Description',
                    labelText: "Description",
                  ),
                  validator: (val)=>val.isEmpty ?'Content is Required':
                  null,
                  
                  onSaved: (val) => _content = val,
                )
                ); 

                final submitButton= Padding(
      padding: EdgeInsets.only(left: 24.0, right: 24.0),
      child: new MyRaisedButton(
                     loading: loading,
                      textColor: colorWhite,
                      onPressed: uploadImage,
                      buttonColor: colorBrown,
                      title: 'Submit',
                      ),
    );          
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: colorBrown),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text("Add News",style: TextStyle(color: Colors.purple),),

      ),
        body: Stack(
      children: <Widget>[
        Form(
            key: _formkey,
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 70,
                ),
                Container(
                  height: 200,
                  child: Center(
                    child: _image == null
                        ? Text('No image selected.')
                        : Image.file(_image),
                  ),
                ),
                headingField,
                contentField,
                SizedBox(height: 30.0,),
                submitButton
                
              ],
            ),
            )
      ],
    ),
    floatingActionButton: FloatingActionButton(
                      heroTag: 'button2',
                      backgroundColor: colorBrown,
                      onPressed: getImageFromGallery,
                      tooltip: 'Pick Image',
                      child: Icon(Icons.wallpaper, color: colorWhite,),
                    ),
    
    );
   
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height - 40);
    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width - (size.width / 4), size.height,
        size.width, size.height - 40);

    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
