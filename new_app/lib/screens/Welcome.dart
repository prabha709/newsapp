

import 'package:flutter/material.dart';
import 'package:new_app/Login/Login.dart';
import 'package:new_app/App_Colors/App_Colors.dart';
import 'package:new_app/Login/NewRegister.dart';
// import 'package:new_app/Login/SignUp.dart';


class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
      body: Form(
              child: Column(
          children:<Widget>[
        PinkRectangle(),
        Logo(),
        LogoText(),
        BounceLoginButton(),
        SizedBox(height: 10.0),
        BounceSignUpButton(),
        SizedBox(height: 10.0),
        ForgotLabel(),
         SizedBox(height: 12.0),

        BottomPinkRectangle(),
        

          ]
              
        ),
      ),
     
      
    );
  }
}

class PinkRectangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  return Transform(
  transform: Matrix4.skewY(0.1)..rotateX(3.14/20.0),
  child:Transform.translate(
    offset: Offset(0.0, -70.0),
  
  
 child: Container(
    height:MediaQuery.of(context).size.height/3.5,
    width: MediaQuery.of(context).size.width,
    padding: const EdgeInsets.all(0.0),
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: pinkGradients,
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      )
    ),
   
  ),
  )
);
  }
}
class BottomPinkRectangle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  return Transform(
  transform: Matrix4.skewY(0.1)..rotateX(3.14/20.0),
  child:Transform.translate(
    offset: Offset(0.0, 70.0),
  
  
 child: Container(
    height: MediaQuery.of(context).size.height/3.5,
    width: MediaQuery.of(context).size.width,
    padding: const EdgeInsets.all(0.0),
    decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: pinkGradients,
        begin: Alignment.topLeft,
        end: Alignment.bottomRight
      )
    ),
   
  ),
  )
);
  }
}
class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.translate(
      offset: Offset(-10.0, -30.0),
    
   child: 
    Center(
      child: Image.asset('assets/logo.png', width: 150.0,),
    )
    );

    }
}
class LogoText extends StatelessWidget{
@override
Widget build (BuildContext context){
return Transform.translate(
      offset: Offset(0.0, -25.0),
child:
Center(child: 
Text('Welcome to Best News', style: TextStyle(fontWeight: FontWeight.bold),),
),
);
}


}

class BounceLoginButton extends StatefulWidget {
  @override
  _BounceLoginButtonState createState() => _BounceLoginButtonState();
}

class _BounceLoginButtonState extends State<BounceLoginButton>with SingleTickerProviderStateMixin {
  AnimationController _animationController;

   @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 600),
      lowerBound: 0.0,
      upperBound: 0.1,
    );
    _animationController.addListener(() {
      setState(() {});
    });
  }
  @override
  Widget build(BuildContext context) {
     double scale = 1 - _animationController.value;
    return Center(
      child: GestureDetector(
        onTapUp: _onTapUp,
        onTapDown: _onTapDown,
        child: Transform.scale(
          scale: scale,
          child: Container(
            width: MediaQuery.of(context).size.width/1.2,
            height: MediaQuery.of(context).size.height/17,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: pinkGradients
              ),
              borderRadius: BorderRadius.circular(38.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  offset:Offset(0.0, 2.0),
                  blurRadius: 5.0,
                  spreadRadius: 0.25
                  ),
                
              ]
            ),
            child: Text("LOGIN", style: TextStyle(fontWeight: FontWeight.bold, color: Theme.of(context).buttonColor),),
            alignment: Alignment.center,
            
            ),
        ),
      ),
      
    );
  }
    void _onTapDown(TapDownDetails details) {
    _animationController.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _animationController.reverse();
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder:(context)=> Login()));
  
    
  }

}
class BounceSignUpButton extends StatefulWidget {
  @override
  _BounceSignUpButtonState createState() => _BounceSignUpButtonState();
}

class _BounceSignUpButtonState extends State<BounceSignUpButton>with SingleTickerProviderStateMixin {
  AnimationController _animationController;

   @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
      lowerBound: 0.0,
      upperBound: 0.1,
    );
    _animationController.addListener(() {
      setState(() {});
    });
  }
  @override
  Widget build(BuildContext context) {
     double scale = 1 - _animationController.value;
    return Center(
      child: GestureDetector(
        onTapUp: _onTapUp,
        onTapDown: _onTapDown,
        child: Transform.scale(
          scale: scale,
          child: Container(
            width: MediaQuery.of(context).size.width/1.2,
            height: MediaQuery.of(context).size.height/17,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: pinkGradients,
              ),
              borderRadius: BorderRadius.circular(38.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  offset:Offset(0.0, 2.0),
                  blurRadius: 5.0,
                  spreadRadius: 0.25
                  ),
                
              ]
            ),
            child: Text("SIGN UP", style: TextStyle(fontWeight: FontWeight.bold, color: Theme.of(context).buttonColor),),
            alignment: Alignment.center,
            ),
        ),
      ),
      
    );
  }
    void _onTapDown(TapDownDetails details) {
    _animationController.forward();
    }

  void _onTapUp(TapUpDetails details) {
    _animationController.reverse();
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(builder:(context)=> NewRegister()));
  
    
  }

}
class ForgotLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
                
                child: 
                    InkWell(
                      onTap: (){
                        
                      },
                      child: Text("Click here to reset Password",
                          style:
                              TextStyle(fontSize: 15, color: Theme.of(context).buttonColor)),
                    ),
                  
              );
  }
}
