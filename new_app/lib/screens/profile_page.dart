import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {


bool isSwitched = false;
bool isSwitched1 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
              children:<Widget>[ Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 80),
             
            ),
            
            Align(
              alignment: Alignment.center,
              child: SizedBox(
                
                height: 300,
                width: 300,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    //orange
                    Positioned(
                      height: 100,
                      width: 100,
                      top: 10,
                      right: 10,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xffFE7227),
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                                colors: [Color(0xffFE7227), Color(0xffFEA120)],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xffFE7227),
                                  offset: Offset(0, 10),
                                  blurRadius: 20)
                            ]),
                      ),
                    ),
                    //purple
                    Positioned(
                      height: 100,
                      width: 100,
                      bottom: 10,
                      left: 10,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                                colors: [Color(0xffB767FC), Color(0xffD765FB)],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xffB767FC),
                                  offset: Offset(0, 10),
                                  blurRadius: 20)
                            ]),
                      ),
                    ),
                    //green
                    Container(
                      height: 200,
                      width: 200,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(
                            end: Alignment.topCenter,
                            begin: Alignment.bottomCenter,
                            colors: [Color(0xff3DD92E), Color(0xff8BFB6B)]),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 30),
                              color: Color(0xFF3DD92E).withAlpha(100),
                              blurRadius: 40)
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: Colors.white,
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(0, 4),
                                      color: Colors.black38,
                                      blurRadius: 5)
                                ]),
                            child: ClipOval(
                              // child: Image.asset(
                              //   "assets/images/logo.png",
                              //   fit: BoxFit.cover,
                              //   width: 100,
                              //   height: 100,
                              // ),
                            ),
                          ),
                          Text(
                            "Mario Hero",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w400),
                          )
                        ],
                      ),
                    ),
                    //blue
                    Positioned(
                      height: 35,
                      width: 35,
                      top: 40,
                      left: 40,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                                colors: [Color(0xff0894FF), Color(0xff05BFFA)],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xff0894FF),
                                  offset: Offset(0, 5),
                                  blurRadius: 10)
                            ]),
                      ),
                    ),
                    //red
                    Positioned(
                      height: 15,
                      width: 15,
                      bottom: 40,
                      right: 40,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xffFF3443),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xffFF3443),
                                  offset: Offset(0, 2),
                                  blurRadius: 5)
                            ]),
                      ),
                    ),
                  ],
                ),
              ),
            ),
             Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.phone),
                    title: Text("Phone Number"),
                    subtitle: Text("data"),
                    
                  )
                ],
              ),

            
            Padding(
              padding: EdgeInsets.only(left:18.0,top: 10.0),
              child: Text("App Settings",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
            ),
            Padding(
              padding: EdgeInsets.only(left:18.0,),
                          child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Notification"),
                  Switch(
  value: isSwitched,
  onChanged: (value) {
    setState(() {
      isSwitched = value;
    });
  },
  activeTrackColor: Colors.lightGreenAccent, 
  activeColor: Colors.green,
),
                ],
              ),
            ),
            
            Padding(
              padding: EdgeInsets.only(left:18.0,),
                          child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Night Mode"),
                  Switch(
  value: isSwitched1,
  onChanged: (value) {
    setState(() {
      isSwitched1 = value;
    });
  },
  activeTrackColor: Colors.lightGreenAccent, 
  activeColor: Colors.green,
),
                ],
              ),
            ),
             Padding(
              padding: EdgeInsets.only(left:1.0,),
                          child: 
                  FlatButton(
                    child: Text("Bookmarks"),
                    onPressed: (){},
                  ),
                
            ),
            
            Padding(
              padding: EdgeInsets.only(left:18.0,top: 10.0),
              child: Text("Contact Details",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
            ),
Padding(
              padding: EdgeInsets.only(left:1.0,),
                          child: 
                  FlatButton(
                    child: Text("About us"),
                    onPressed: (){},
                  ),
                
            ),
            Padding(
              padding: EdgeInsets.only(left:1.0,),
                          child: 
                  FlatButton(
                    child: Text("Feed Back"),
                    onPressed: (){},
                  ),
                
            ),
            Padding(
              padding: EdgeInsets.only(left:1.0,),
                          child: 
                  FlatButton(
                    child: Text("Terms & Conditions"),
                    onPressed: (){},
                  ),
                
            ),

          ],
        ),
              ]
      ),
      
    );
  }
}

enum PaintType { left, right }

class ArcPainter extends CustomPainter {
  final PaintType type;
  ArcPainter({this.type});
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Color(0xffFFC300)
      ..style = PaintingStyle.fill;

    if (type == PaintType.right) {
      paint.color = Color(0xffA562FF);
      canvas.drawCircle(Offset(10, 80), 60, paint);
    } else {
      canvas.drawCircle(Offset(-10, -10), 130, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
