import 'package:cloud_firestore/cloud_firestore.dart';


class UserManagement {
  storeNewUser(user, firstname, lastname, phoneNumber ) {
    Firestore.instance.collection('/users').document(user.uid).setData({
      'email':user.email,
      'uid':user.uid,
      'firstname':firstname,
      'lastname':lastname,
       'phonenumber':phoneNumber,
      

    }).then((value) {

    }).catchError((e) {
          print(e);
        });
  }
}
class ClientManagement{
  storeNewClient(user,displayname, phonenumber,email){
    Firestore.instance.collection('/clients').document(user.uid).setData({
      'email':user.email,
      'displayname':user.displayname,
      'phonenumber':user.phonenumber,
      }).then((value){

      }).catchError((e){
        print(e);
      });
  }
}