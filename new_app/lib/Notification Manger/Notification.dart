import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io';

class PushNotificationService{
final FirebaseMessaging _fcm = FirebaseMessaging();
Future initialise() async{
  if(Platform.isIOS){
    _fcm.requestNotificationPermissions(IosNotificationSettings());
  }
  _fcm.configure(
     onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        // _showItemDialog(message);
      },
      // onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // _navigateToItemDetail(message);
      },

  );


}
}